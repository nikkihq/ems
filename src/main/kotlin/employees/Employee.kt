package employees

import org.xml.sax.helpers.LocatorImpl
import java.time.LocalDate

abstract class Employee (
    override var firstName: String,
    override var lastName: String,
    var department: String,
    val hiringDate: LocalDate,
    var location: LocationDetails
): EmployeeInterface {

    var generateID: String = ""

    protected var governmentId = "SSS"
    private var officeId = "abcd123"

    //Creates Data of Employees
    fun employeeData() = println("Employee Name: $firstName $lastName from $department department")


    init {
        this.generateID = "${firstName.substring(0, 2).uppercase()}-${
            lastName.substring(0, 2).uppercase()
        }-${(10000000 until 99999999).random()}"
    }
}

    class Person(override var firstName: String, override var lastName: String): EmployeeInterface {
        override fun doSomething(): String {
            TODO("Not yet implemented")
        }

    }



