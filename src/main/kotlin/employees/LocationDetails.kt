package employees


/*
* A class containing all the details about the office
  location of an employee
  * city, province, region, exact address
* */

// It is used for holding data
// Holder of data - use when pure data are going to be in the class

// Data class -> Automatic string conversion (toString())
// equal() checks the actual data
// we can use copy()
// Doesnt need to repeat the instantiation over and over

data class LocationDetails(
    var city: String,
    var province: String,
    var Region: String,
    var exactAddress: String
)

//Show the data on how it looks like in console
//Stringied value of the instantiated object (JSON like)
//LocationDetails(city=Makati, province=Metro Manila, Region=NCR, exactAddress=1124 XYZ Building, Legaspi Village)

//Map version of the object above
//{city=Makati, province=Metro Manila, Region=NCR, exactAddress=1124 XYZ Building, Legaspi Village}
