package employees

import java.time.LocalDate

fun main(){
    //val employee1 = RankAndFileEmployee("Amy", "Carter", "IT", LocalDate.now())
    //val employee2 = AdminEmployee("Brandon", "Cruz", "HR", LocalDate.now())

//    val employee2 = AdminEmployee("Brandon", "Cruz", "HR", LocalDate.now()).apply {
//        this. firstName = firstName.uppercase()

    //with, apply, let, run - Higher order function (modifies the object)
//   with(employee2){
//       println(firstName)
//       println(lastName)
//       println(generateID)
//       println(department)
//       println(isRankAndFile)
//       println(isAdmin)
//   }

//    Returns what is modification inside - Applies whatever the modification is done in object
//    println(employee2.firstName)
//    employee2.apply {
//        this. firstName = firstName.uppercase()
//    }
//    println(employee2.firstName)

//    val employee1 = RankAndFileEmployee("Amy", "Carter", "IT", LocalDate.now())
//    println(employee1)
//
////    val employee2 = RankAndFileEmployee("Amy", "Carter", "IT", LocalDate.now())
////    println(employee2)
////  Checks the reference not the values
////    println(employee1 == employee2)
//
//    val address1 = LocationDetails(
//        "Makati",
//        "Metro Manila",
//        "NCR",
//        "1124 XYZ Building, Legaspi Village"
//    )
//    println(address1)

//    val address2 = address1.copy(
//        exactAddress = "1623 HJK Building, Salcedo Village"
//    )
//    println(address1)
//    println(address2)


//  Checks the actual data
//  println(address1 == address2)

// Add the DATA CLASS to the employee details to use the address
    val address1 = LocationDetails(
        "Makati",
        "Metro Manila",
        "NCR",
        "1124 XYZ Building, Legaspi Village"
    )
    val employee1 = RankAndFileEmployee("Amy", "Carter", "IT", LocalDate.now(), address1)
    println(employee1.department)

    val admin = AdminEmployee("Lito", "Delipan", "Executive", LocalDate.now(), address1)
    admin.changePermission("isAdmin")
    println(admin.toString())


//    Visibility modifiers
//    Public - Default visibility, it is use when overriding properties in inheritance
//    Private - Can only be seen inside the same class
//    Protected - Same as private but it's also visible in the subclasses
//    Internal - Only visible in the same module


}