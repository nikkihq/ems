package employees

import java.time.LocalDate

class AdminEmployee(firstName: String,
                    lastName: String,
                    department: String,
                    hiringDate: LocalDate,
                    location: LocationDetails
): Employee(firstName, lastName, department, hiringDate, location
){
    var isRankAndFile: Boolean = false
        private set
    var isAdmin: Boolean = true
        private set

    fun changePermission(permissionType: String){
        if(isAdmin == true){
            print("Change permission? Y or N: ")
            var adminRespond = readLine()!!
            if(adminRespond == "Y"){
                isRankAndFile = false
                isAdmin = true
            }
            else{
                println("No Permission is changed")
            }
        }else{
            isRankAndFile = true
            isAdmin = false
        }
    }

//    fun changePermissions(permissionType: String){
//        when(permissionType){
//            "admin" -> {
//                isRankAndFile = false
//                isAdmin = true
//            }
//            "rankAndFile" -> {
//                isRankAndFile = true
//                isAdmin = false
//            }
//        }
//    }


    //Return value is admin
    override fun doSomething(): String = isAdmin.toString()
}